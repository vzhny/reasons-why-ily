import React from 'react';
import { Spring } from 'react-spring';
import last from 'lodash/last';
import split from 'lodash/split';
import PropTypes from 'prop-types';
import styles from './Item.module.scss';

const Item = ({ item }) => {
  const { length } = item;
  const text = item.slice(0, length - 2);
  const emoji = last(split(item, ' '));

  return (
    <>
      <Spring
        from={{ opacity: 0, transform: 'translate3d(0,-40px,0)' }}
        to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
        delay={1500}
      >
        {props => (
          <h1 style={props} className={styles.Item}>
            {text}
            <Spring from={{ opacity: 0 }} to={{ opacity: 1 }} delay={2000}>
              {innerProps => (
                <span style={innerProps} className={styles.Emoji} role="img" aria-label="Emoji">
                  {emoji}
                </span>
              )}
            </Spring>
          </h1>
        )}
      </Spring>
    </>
  );
};

Item.propTypes = {
  item: PropTypes.string.isRequired,
};

export default Item;
