import React from 'react';
import { Spring } from 'react-spring';
import PropTypes from 'prop-types';
import styles from './Header.module.scss';

const Header = ({ index }) => (
  <header className={styles.Header}>
    <Spring
      from={{ opacity: 0, transform: 'translate3d(0,-40px,0)' }}
      to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
    >
      {props => (
        <p style={props} className={styles.MainText}>
          Reason #
          <Spring
            from={{ number: 1 }}
            to={{ number: index + 1 }}
            config={{
              friction: 40,
              velocity: 50,
            }}
          >
            {innerProps => <span style={innerProps}>{innerProps.number.toFixed()}</span>}
          </Spring>
        </p>
      )}
    </Spring>
  </header>
);

Header.propTypes = {
  index: PropTypes.number.isRequired,
};

export default Header;
