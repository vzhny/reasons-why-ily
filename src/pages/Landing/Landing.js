import React from 'react';
import Item from '@/components/Item/Item';
import PropTypes from 'prop-types';

const Landing = ({ item }) => <Item item={item} />;

Landing.propTypes = {
  item: PropTypes.string.isRequired,
};

export default Landing;
