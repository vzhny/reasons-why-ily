import React from 'react';

const About = () => (
  <p
    style={{
      fontSize: '1.2rem',
      width: '650px',
      textAlign: 'center',
    }}
  >
    Inspired by Kent C. Dodd's script that messages his SO, I wanted to make a really small web app
    that shows one thing I love about my SO on each visit or refresh.{' '}
    <span role="img" aria-label="Smiling Face with Smiling Eyes">
      😊
    </span>
  </p>
);

export default About;
