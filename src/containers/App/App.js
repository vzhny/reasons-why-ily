import React, { Component } from 'react';
import { Router } from '@reach/router';
import Header from '@/components/Header/Header';
import Landing from '@/pages/Landing/Landing';
import About from '@/pages/About/About';
import styles from './App.module.scss';

export default class App extends Component {
  state = {
    items: [
      '"Don\'t be jealous!" 😛',
      'Smooch, smooch! 😚',
      'Your dimples. 😊',
      'The "seeing Gina from across the room" face. 😬',
      'mimimi! 😍',
      'Making breakfast together. 😊',
      'Over-the-phone movie night. 😊',
      'Warming up my cold feet. 😯',
      'Humoring all the "Lets Go Mets" chants. 😜',
      'Your hands running through my hair. 😊',
      'Your impromptu grammar lessons. 🤓',
      'Your laser focus when we cook or make something. 😊',
      'All your sketches, paintings, and art projects. 😍',
      'Your monkaS face. 😳',
      'The way you joke with Marlon. 🤣',
      'How much you both old and young pups. 🐶',
      'The fact you introduced me to my thicc son Lola. 🙀',
      'How you introduced me to visiting and appreciating museums. 🙏',
      'Free coffee and fruit at your office\'s break room. 😝',
      'Getting to play board games with your little cousins. 😄',
      'How we send each other those bear couple stickers. 😊',
      'Sending each other "us" posts on ig. 😍',
      'Us both gasping and saying, "look!" when we see any pup on the street. 🐶',
      'How we always remember all our pseudo-dates. 😍',
      'How much you like my neck and cheek smooches. 😍',
      'All our just-shopping-at-Target dates. 😊',
      'How you fall asleep every time we try and watch a movie. 😴',
      'The three beauty marks on your beautiful face. 😍',
      'Helping me sneak in a salad when we went to see Moana in theaters. 🤣',
      '"It\'s my house, I get to pick the movie." 😛',
      'Playing Smash Ultimate with me and Marlon. 😊',
      'Getting to share that cubano sandwich from Constantine\'s. 😮',
      'You supported me every step of the way during my CS degree. 😍',
      'Singing along to 92.3. 😛',
      'Humoring me whenever I start talking about something to do with tech. 😛',
      'All the pet names I call you, and you call me. 😍',
      'How we\'re always up for any kind of cuisine. 🤤',
      'The lunchtime phone calls. 😍',
      '"I\'ll be there in two minutes." 😊',
      'Getting to help out with your work posts. 😄',
      'Your finger wiggle dance. 😜',
      'How you introduced me to the turkey-salami-cheddar sandwich order. 🤯',
      'Getting to work together at your office when it\'s empty on Fridays. 😊',
      'Letting me do that silly puppy gnaw thing. 🐶',
    ],
    index: 0,
  };

  componentDidMount() {
    const randomIndex = this.getRandomIndex();

    this.setState({ index: randomIndex });
  }

  getRandomIndex = () => {
    const index = this.state.items.length;

    return Math.floor(Math.random() * Math.floor(index));
  };

  render() {
    const { index, items } = this.state;

    return (
      <div className={styles.App}>
        <Header index={index} />
        <Router className={styles.Main}>
          <Landing default path="/" item={items[index]} />
          <About path="/about" />
        </Router>
      </div>
    );
  }
}
